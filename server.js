const http = require('http');
const url = require('url');
const fetch = require('node-fetch');
const moment = require('moment');

const hostname = '127.0.0.1';
const port = 3000;

const ranges = (date, start, end) => {
    if (start && end) {
        return moment(date).isBetween(start, end, 'day', '[]')
    }

    if (!start) {
        return moment(date).isBefore(end, 'day');
    }

    if (!end) {
        return moment(date).isAfter(start, 'day');
    }
}

const flatData = (data) => {
    let reducer = reduceChats(data);
    reducer.forEach(reducer => delete reducer.date);
    return reducer;
}

const reduceChats = (data, reducer = []) => {
    if (!data.length) {
        return reducer;
    }
    const dataItem = data.shift();
    const removeIds = [];
    data.forEach((item, index) => {
        if (item.websiteId === dataItem.websiteId) {
            removeIds.push(index);
            dataItem.chats += item.chats;
            dataItem.missedChats += item.missedChats;
        }
    })

    removeIds.forEach(id => {data[id] = undefined});
    data = data.filter(data => data !== undefined);

    reducer.push(dataItem);
    return reduceChats(data, reducer);
}

const processData = (data, start = undefined, end = undefined) => {
    if (!data.length || !start && !end) {
        return data;
    }

    start = start ? moment(start) : start;
    end = end ? moment(end) : end;

    const filtered = [];
    for(let i = 0; i < data.length; i++) {
        if (ranges(data[i].date, start, end)) {
            filtered.push(data[i]);
        }
    }
    return filtered;
}

async function loadData(url) {
    return await fetch(url)
        .then(res => res.json())
        .then(res => res);
}

const server = http.createServer((req, res) => {
    const query = url.parse(req.url,true).query;
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    loadData('https://bitbucket.org/api/2.0/snippets/tawkto/aA8zqE/4f62624a75da6d1b8dd7f70e53af8d36a1603910/files/webstats.json')
        .then(response => {
            response = processData(response, query.start, query.end);
            response = flatData(response);
            res.end(JSON.stringify(response))
        });
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});